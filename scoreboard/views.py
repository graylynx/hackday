from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.utils import timezone

from operator import itemgetter

from .models import Flag


def main(request):
    def post(req_flag, req_user):
        if req_user.is_authenticated() and req_user.is_active:
            try:
                flag = Flag.objects.filter(key=req_flag, owned_by=None)[0]
                flag.owned_by = req_user
                flag.owned_date = timezone.now()
                flag.save()
            except IndexError:
                pass

    def get_scoreboard():
        def multikeysort(items, columns):
            'https://wiki.python.org/moin/SortingListsOfDictionaries'
            from operator import itemgetter
            comparers = [ ((itemgetter(col[1:].strip()), -1) if col.startswith('-') else (itemgetter(col.strip()), 1)) for col in columns]
            def comparer(left, right):
                for fn, mult in comparers:
                    result = cmp(fn(left), fn(right))
                    if result:
                        return mult * result
                else:
                    return 0
            return sorted(items, cmp=comparer)

        scoreboard = []
        for user in User.objects.filter(is_superuser=False, is_active=True):
            user_info = {}
            user_info['username'] = user.username
            flags = user.flag_set.all()
            if len(flags) > 0:
                user_info['score'] = sum([f.score for f in flags])
                user_info['flags'] = len(flags)
                user_info['activity'] = flags.order_by('-owned_date')[0].owned_date
            else:
                # to prevent a exception
                user_info['score'] = 0
                user_info['flags'] = 0
                user_info['activity'] = None
            scoreboard.append(user_info)
        return multikeysort(scoreboard, ['-score', '-flags', 'activity'])

    # verify a flag
    req_flag = request.POST.get('flag')
    if req_flag:
        post(req_flag, request.user)

    # make a response body
    context = {}
    if request.user.is_superuser:
        context['flag_list'] = Flag.objects.filter(owned_by=None)
    else:
        context['user_list'] = get_scoreboard()
    return render(request, 'main.html', context)


def my_login(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(username=username, password=password)
    if user and user.is_active:
        login(request, user)
    return redirect('/')


def my_logout(request):
    logout(request)
    return redirect('/')


def register(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        if not username or not password:
            return redirect(register)
        try:
            User.objects.create_user(username=username, password=password).save()
            user = authenticate(username=username, password=password)
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user)
        except:
            raise
            return redirect(register)
        return redirect('/')
    else:
        return render(request, 'register.html')
