from django.db import models
from django.contrib.auth.models import User

from random import choice, randint
from string import digits


class FlagManager(models.Manager):
    def create_many(self, num):
        for i in range(num):
            key = ''.join(choice(digits) for _ in range(16))
            score = randint(1, 10)
            flag = self.create(key=key, score=score)
            flag.save()


class Flag(models.Model):
    key = models.CharField(max_length=16, unique=True)
    score = models.SmallIntegerField()
    owned_by = models.ForeignKey(User, blank=True, null=True)
    owned_date = models.DateTimeField(blank=True, null=True)

    objects = FlagManager()

    def __str__(self):
        return self.key
